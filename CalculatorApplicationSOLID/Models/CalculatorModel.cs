﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic;

namespace CalculatorApplicationSOLID.Models
{
    public class CalculatorModel
    {
        public int Numberone { get; set; }
        public int Numbertwo { get; set; }
        public int Finalresult { get; set; }
    }
}