﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CalculatorApplicationSOLID.Models;
using StructureMap;

namespace CalculatorApplicationSOLID.Controllers
{
    public class CalculatorController : Controller
    {
        private readonly ISelectOperation _operator;
        public CalculatorController(ISelectOperation operate)
        {
            _operator = operate;
        }
        [HttpGet]
        public ActionResult Index()
        {
            return View(new CalculatorModel());
        }
        [HttpPost]
        public ActionResult Index(CalculatorModel values, string calculate)
        {
            values.Finalresult = _operator.Check(values.Numberone, values.Numbertwo, calculate);
            return View(values);
        }
    }
}