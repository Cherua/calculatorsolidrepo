﻿using BusinessLogic;
using BusinessLogic.Interfaces;
using StructureMap;



namespace CalculatorApplicationSOLID.Controllers
{
    public class SelectOperation : ISelectOperation
    {
        private readonly IAdditionOfTwoNumbers _additionoftwonumbers;
        private readonly ISubtractionOfTwoNumbers _subtractionoftwonumbers;
        private readonly IMultiplicationOfTwoNumbers _multiplicationoftwonumbers;
        private readonly IDivisionOfTwoNumbers _divisionoftwonumbers;
        private readonly IModulusOfTwoNumbers _modulusoftwonumbers;

        public SelectOperation(IAdditionOfTwoNumbers additionoftwonumbers, ISubtractionOfTwoNumbers subtractionoftwonumbers, 
            IMultiplicationOfTwoNumbers multiplicationoftwonumbers, IDivisionOfTwoNumbers divisionoftwonumbers, 
            IModulusOfTwoNumbers modulusoftwonumbers)
        {
            _additionoftwonumbers = additionoftwonumbers;
            _subtractionoftwonumbers = subtractionoftwonumbers;
            _multiplicationoftwonumbers = multiplicationoftwonumbers;
            _divisionoftwonumbers = divisionoftwonumbers;
            _modulusoftwonumbers = modulusoftwonumbers;
        }
        public int Check(int firstnumber, int secondnumber, string symbol)
        {
            int finalvalue = 0;
            if (symbol == "add")
            {
                finalvalue =  _additionoftwonumbers.CalculationAdd(firstnumber, secondnumber);
            }
            if (symbol == "sub")
            {
                finalvalue =  _subtractionoftwonumbers.CalculationSubtraction(firstnumber, secondnumber);
            }
            if (symbol == "mul")
            {
                finalvalue = _multiplicationoftwonumbers.CalculationMultiplication(firstnumber, secondnumber);
            }
            if (symbol == "div")
            {
                finalvalue = _divisionoftwonumbers.CalculationDivision(firstnumber, secondnumber);
            }
            if (symbol == "mod")
            {
                finalvalue = _modulusoftwonumbers.CalculationModulus(firstnumber, secondnumber);
            }
            return finalvalue;
        }
    }
}