﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;

namespace CalculatorApplicationSOLID.Controllers
{
    public interface ISelectOperation
    {
        int Check(int firstnumber, int secondnumber, string symbol);
    }
}
