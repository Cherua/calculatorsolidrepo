﻿
using BusinessLogic.Interfaces;

namespace BusinessLogic.ClassesWorking
{
    public class AdditionOfTwoNumbers : IAdditionOfTwoNumbers
    {
        public int CalculationAdd(int firstnumber,int secondnumber)
        {
            return firstnumber + secondnumber;
        }
    }
}
