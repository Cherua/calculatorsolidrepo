﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interfaces;

namespace BusinessLogic.ClassesWorking
{
    public class DivisionOfTwoNumbers : IDivisionOfTwoNumbers
    {
        public int CalculationDivision(int firstnumber, int secondnumber)
        {
            return firstnumber / secondnumber;
        }
    }
}
