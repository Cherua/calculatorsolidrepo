﻿using BusinessLogic.Interfaces;

namespace BusinessLogic.ClassesWorking
{
    public class SubtractionOfTwoNumbers : ISubtractionOfTwoNumbers
    {
        public int CalculationSubtraction(int firstnumber, int secondnumber)
        {
            return firstnumber - secondnumber;
        }
    }
}
