﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.ClassesWorking;

namespace BusinessLogic.Interfaces
{
    public interface IAdditionOfTwoNumbers
    {
        int CalculationAdd(int firstnumber, int secondnumber);
    }
}
