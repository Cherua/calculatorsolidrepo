﻿using BusinessLogic.ClassesWorking;

namespace BusinessLogic.Interfaces
{
    public interface ISubtractionOfTwoNumbers
    {
        int CalculationSubtraction(int firstnumber, int secondnumber);
    }
}
